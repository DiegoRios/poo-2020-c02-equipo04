package ar.edu.fi.unju.grupo04poo2020.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.fi.unju.grupo04poo2020.entity.Ubicacion;

public interface UbicacionRepository extends JpaRepository<Ubicacion, Integer>{
	Optional<Ubicacion>findById(Integer id);

}
