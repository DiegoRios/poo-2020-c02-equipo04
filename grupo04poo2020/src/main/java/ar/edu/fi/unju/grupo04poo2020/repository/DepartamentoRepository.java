package ar.edu.fi.unju.grupo04poo2020.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.fi.unju.grupo04poo2020.entity.Departamento;

public interface DepartamentoRepository extends JpaRepository<Departamento, Integer>{
	List<Departamento> findByManager(Integer id);
	Departamento findByDepartamentoId(Integer id);

}
