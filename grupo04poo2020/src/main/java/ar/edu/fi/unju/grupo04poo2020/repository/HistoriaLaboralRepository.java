package ar.edu.fi.unju.grupo04poo2020.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.fi.unju.grupo04poo2020.entity.Empleado;
import ar.edu.fi.unju.grupo04poo2020.entity.HistoriaLaboral;

public interface HistoriaLaboralRepository extends JpaRepository<HistoriaLaboral, Integer>{
	List<Empleado> findByEmpleado(Empleado empleado);
}
