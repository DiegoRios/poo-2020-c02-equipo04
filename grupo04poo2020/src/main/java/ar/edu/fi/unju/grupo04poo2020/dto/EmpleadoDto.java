package ar.edu.fi.unju.grupo04poo2020.dto;

import java.io.Serializable;
import java.util.Date;


public class EmpleadoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nombre;
	private String apellido;
	private String email;
	private String telefono;
	private Date fechaAlquiler;
	//private PuestoLaboralDto puestoLaboral;
	private String puestoLaboral;
	private Double salario;
	private Double comision;
	//private EmpleadoDto manager;
	private Integer manager;
	//private DepartamentoDto departamento;
	private Integer departamento;
	
	
	public EmpleadoDto() {
		super();
	}
	
	

	/*public EmpleadoDto(Integer id, String nombre, String apellido, String email, String telefono, Date fechaAlquiler,
			PuestoLaboralDto puestoLaboral, Double salario, Double comision, EmpleadoDto manager,
		    DepartamentoDto departamento) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.fechaAlquiler = fechaAlquiler;
		this.puestoLaboral = puestoLaboral;
		this.salario = salario;
		this.comision = comision;
		this.manager = manager;
		this.departamento = departamento;
	}*/



	/*public EmpleadoDto(String nombre, String apellido, String email, String telefono, Date fechaAlquiler,
			PuestoLaboralDto puestoLaboral, Double salario, Double comision, EmpleadoDto manager,
			DepartamentoDto departamento) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.fechaAlquiler = fechaAlquiler;
		//this.puestoLaboral = puestoLaboral;
		this.puestoLaboral = puestoLaboral;
		this.manager = manager;
		this.salario = salario;
		this.comision = comision;
		//this.manager = manager;
		//this.departamento = departamento;
		this.departamento = departamento;
	}*/

	public EmpleadoDto(String nombre, String apellido, String email, String telefono, Date fechaAlquiler,
			String puestoLaboral, Double salario, Double comision, Integer manager, Integer departamento) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.fechaAlquiler = fechaAlquiler;
		this.puestoLaboral = puestoLaboral;
		this.salario = salario;
		this.comision = comision;
		this.manager = manager;
		this.departamento = departamento;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getApellido() {
		return apellido;
	}



	public void setApellido(String apellido) {
		this.apellido = apellido;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getTelefono() {
		return telefono;
	}



	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}



	public Date getFechaAlquiler() {
		return fechaAlquiler;
	}



	public void setFechaAlquiler(Date fechaAlquiler) {
		this.fechaAlquiler = fechaAlquiler;
	}



	public Double getSalario() {
		return salario;
	}



	public void setSalario(Double salario) {
		this.salario = salario;
	}



	public Double getComision() {
		return comision;
	}



	public void setComision(Double comision) {
		this.comision = comision;
	}

	public String getPuestoLaboral() {
		return puestoLaboral;
	}

	public void setPuestoLaboral(String puestoLaboral) {
		this.puestoLaboral = puestoLaboral;
	}

	public Integer getManager() {
		return manager;
	}

	public void setManager(Integer manager) {
		this.manager = manager;
	}

	public Integer getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Integer departamento) {
		this.departamento = departamento;
	}



	/*public EmpleadoDto getManager() {
		return manager;
	}


    public void setManager(EmpleadoDto manager) {
		this.manager = manager;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public PuestoLaboralDto getPuestoLaboral() {
		return puestoLaboral;
	}

    public void setPuestoLaboral(PuestoLaboralDto puestoLaboral) {
		this.puestoLaboral = puestoLaboral;
	}

	public DepartamentoDto getDepartamento() {
		return departamento;
	}

	public void setDepartamento(DepartamentoDto departamento) {
		this.departamento = departamento;
	}*/
	
	

}
