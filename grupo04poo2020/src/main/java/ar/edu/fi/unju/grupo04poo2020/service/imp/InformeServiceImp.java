package ar.edu.fi.unju.grupo04poo2020.service.imp;

import java.util.List;

import org.springframework.stereotype.Service;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.service.InformeService;
import ar.edu.fi.unju.grupo04poo2020.view.util.Informe;

@Service
public class InformeServiceImp implements InformeService{
	private Informe informe;

	@Override
	public Boolean generarInforme(EmpleadoDto empleadoDto) {
		return informe.generarInforme(empleadoDto);
	}

	@Override
	public void setInforme(Informe informe) {
		this.informe = informe;
		
	}

	@Override
	public Boolean generarInformeExcel(List<EmpleadoDto> empleados) {
		return informe.generarInformeExcel(empleados);
	}

}
