package ar.edu.fi.unju.grupo04poo2020.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.fi.unju.grupo04poo2020.entity.Empleado;

public interface EmpleadoRepository extends JpaRepository<Empleado, Integer> {
	List<Empleado> findByManager(Integer id);
	Empleado findByEmpleadoId(Integer id);
	List<Empleado> findByDepartamento(Integer id);
}
