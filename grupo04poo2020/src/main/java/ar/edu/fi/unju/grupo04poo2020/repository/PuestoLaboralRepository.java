package ar.edu.fi.unju.grupo04poo2020.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.fi.unju.grupo04poo2020.entity.PuestoLaboral;

public interface PuestoLaboralRepository extends JpaRepository<PuestoLaboral, String> {
	PuestoLaboral findByPuestoLaboralId(String id);
}