package ar.edu.fi.unju.grupo04poo2020.service.imp;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.fi.unju.grupo04poo2020.dto.RolDto;
import ar.edu.fi.unju.grupo04poo2020.entity.Rol;
import ar.edu.fi.unju.grupo04poo2020.repository.RolRepository;
import ar.edu.fi.unju.grupo04poo2020.service.RolService;

@Service
public class RolServiceImp implements RolService {
	private ModelMapper mapper = new ModelMapper();
	@Autowired
	private RolRepository rolRepo;

	@Override
	public RolDto get(Integer id) {
		Optional<Rol> rol = rolRepo.findById(id);
		RolDto rolDto = new RolDto();
		mapper.map(rol,rolDto);
		return rolDto;
	}

	@Override
	public RolDto insert(RolDto rolDto) {
		Rol rol = new Rol();
		mapper.map(rolDto, rol);
		rolRepo.save(rol);
		return rolDto;
	}

}
