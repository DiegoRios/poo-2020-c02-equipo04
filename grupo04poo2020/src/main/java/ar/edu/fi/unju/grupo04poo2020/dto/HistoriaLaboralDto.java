package ar.edu.fi.unju.grupo04poo2020.dto;

import java.io.Serializable;
import java.util.Date;


public class HistoriaLaboralDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private EmpleadoDto empleado;
	private Date fechaInicio;
	private Date fechaFinal;
	private PuestoLaboralDto puestoLaboral;
	private DepartamentoDto departamento;
	
	
	
	
	public HistoriaLaboralDto() {
		super();
	}




	public HistoriaLaboralDto(EmpleadoDto empleado, Date fechaInicio, Date fechaFinal, PuestoLaboralDto puestoLaboral,
			DepartamentoDto departamento) {
		super();
		this.empleado = empleado;
		this.fechaInicio = fechaInicio;
		this.fechaFinal = fechaFinal;
		this.puestoLaboral = puestoLaboral;
		this.departamento = departamento;
	}




	public EmpleadoDto getEmpleado() {
		return empleado;
	}




	public void setEmpleado(EmpleadoDto empleado) {
		this.empleado = empleado;
	}




	public Date getFechaInicio() {
		return fechaInicio;
	}




	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}




	public Date getFechaFinal() {
		return fechaFinal;
	}




	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}




	public PuestoLaboralDto getPuestoLaboral() {
		return puestoLaboral;
	}




	public void setPuestoLaboral(PuestoLaboralDto puestoLaboral) {
		this.puestoLaboral = puestoLaboral;
	}




	public DepartamentoDto getDepartamento() {
		return departamento;
	}




	public void setDepartamento(DepartamentoDto departamento) {
		this.departamento = departamento;
	}
	
	
	

}
