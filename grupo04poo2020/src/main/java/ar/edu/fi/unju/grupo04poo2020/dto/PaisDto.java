package ar.edu.fi.unju.grupo04poo2020.dto;

public class PaisDto {
	private String id;
	private String nombre;
	private RegionDto region;
	
	public PaisDto() {
		super();
	}

	public PaisDto(String id, String nombre, RegionDto region) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.region = region;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public RegionDto getRegion() {
		return region;
	}

	public void setRegion(RegionDto region) {
		this.region = region;
	}
	
	
}
