package ar.edu.fi.unju.grupo04poo2020.service;

import ar.edu.fi.unju.grupo04poo2020.dto.RolDto;

public interface RolService {
	RolDto get(Integer id);
	RolDto insert(RolDto rolDto);

}
