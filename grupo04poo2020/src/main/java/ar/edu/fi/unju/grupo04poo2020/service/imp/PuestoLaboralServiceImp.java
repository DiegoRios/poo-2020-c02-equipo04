package ar.edu.fi.unju.grupo04poo2020.service.imp;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.fi.unju.grupo04poo2020.dto.PuestoLaboralDto;
import ar.edu.fi.unju.grupo04poo2020.entity.PuestoLaboral;
import ar.edu.fi.unju.grupo04poo2020.repository.PuestoLaboralRepository;
import ar.edu.fi.unju.grupo04poo2020.service.PuestoLaboralService;

@Service
public class PuestoLaboralServiceImp implements PuestoLaboralService{
	private ModelMapper mapper = new ModelMapper();
	@Autowired
	private PuestoLaboralRepository puestoLaboralRepo; 
	
	@Override
	public PuestoLaboralDto get(String id) {
		Optional<PuestoLaboral> puesto= puestoLaboralRepo.findById(id);
		PuestoLaboralDto puestoDto = new PuestoLaboralDto();
		mapper.map(puesto,puestoDto);
		return puestoDto;
	}

}
