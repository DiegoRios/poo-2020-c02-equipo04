package ar.edu.fi.unju.grupo04poo2020.dto;

public class UbicacionDto {
	private Integer id;
	private String direccion;
	private String codigoPostal;
	private String cuidad;
	private String provinciaEstado;
	private PaisDto pais;
	
	public UbicacionDto() {
		super();
	}

	public UbicacionDto(Integer id, String direccion, String codigoPostal, String cuidad, String provinciaEstado,
			PaisDto pais) {
		super();
		this.id = id;
		this.direccion = direccion;
		this.codigoPostal = codigoPostal;
		this.cuidad = cuidad;
		this.provinciaEstado = provinciaEstado;
		this.pais = pais;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getCuidad() {
		return cuidad;
	}

	public void setCuidad(String cuidad) {
		this.cuidad = cuidad;
	}

	public String getProvinciaEstado() {
		return provinciaEstado;
	}

	public void setProvinciaEstado(String provinciaEstado) {
		this.provinciaEstado = provinciaEstado;
	}

	public PaisDto getPais() {
		return pais;
	}

	public void setPais(PaisDto pais) {
		this.pais = pais;
	}
	
	
	

}
