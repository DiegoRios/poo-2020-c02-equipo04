package ar.edu.fi.unju.grupo04poo2020.service;

import java.util.List;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.view.util.Informe;

public interface InformeService {
	Boolean generarInforme(EmpleadoDto empleadoDto);
	Boolean generarInformeExcel(List<EmpleadoDto> empleados);
	void setInforme(Informe informe);
}       
