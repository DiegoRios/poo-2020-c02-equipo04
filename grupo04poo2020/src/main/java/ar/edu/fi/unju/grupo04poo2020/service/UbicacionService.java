package ar.edu.fi.unju.grupo04poo2020.service;

import ar.edu.fi.unju.grupo04poo2020.dto.UbicacionDto;

public interface UbicacionService {
	UbicacionDto get(Integer id);

}
