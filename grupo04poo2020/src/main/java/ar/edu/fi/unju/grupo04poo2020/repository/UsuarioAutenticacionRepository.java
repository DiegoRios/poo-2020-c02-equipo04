package ar.edu.fi.unju.grupo04poo2020.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.fi.unju.grupo04poo2020.entity.Empleado;
import ar.edu.fi.unju.grupo04poo2020.entity.UsuarioAutenticacion;

public interface UsuarioAutenticacionRepository extends JpaRepository<UsuarioAutenticacion, Integer>{
	UsuarioAutenticacion findByEmpleado(Integer id);
	Optional<UsuarioAutenticacion> findByUsuario(String usuario);
	UsuarioAutenticacion findByPassword(String password);

}
