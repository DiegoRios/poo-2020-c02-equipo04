package ar.edu.fi.unju.grupo04poo2020.dto;

import java.io.Serializable;

public class UsuarioAutenticacionDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String usuario;
	private String password;
	private boolean activo;
	private Integer empleado;
	private Integer rol;
	
	public UsuarioAutenticacionDto() {
		super();
	}

	
	
	public UsuarioAutenticacionDto(String usuario, String password, boolean activo, Integer empleado, Integer rol) {
		super();
		this.usuario = usuario;
		this.password = password;
		this.activo = activo;
		this.empleado = empleado;
		this.rol = rol;
	}



	/*public UsuarioAutenticacionDto(Integer id, String usuario, String password, boolean activo, EmpleadoDto empleado, RolDto rol) {
		super();
		this.id = id;
		this.usuario = usuario;
		this.password = password;
		this.activo = activo;
		this.empleado = empleado;
		this.rol = rol;
	}*/

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public Integer getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Integer empleado) {
		this.empleado = empleado;
	}

	public Integer getRol() {
		return rol;
	}

	public void setRol(Integer rol) {
		this.rol = rol;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
