package ar.edu.fi.unju.grupo04poo2020.service.imp;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.dto.UsuarioAutenticacionDto;
import ar.edu.fi.unju.grupo04poo2020.entity.Empleado;
import ar.edu.fi.unju.grupo04poo2020.entity.Rol;
import ar.edu.fi.unju.grupo04poo2020.entity.UsuarioAutenticacion;
import ar.edu.fi.unju.grupo04poo2020.repository.EmpleadoRepository;
import ar.edu.fi.unju.grupo04poo2020.repository.RolRepository;
import ar.edu.fi.unju.grupo04poo2020.repository.UsuarioAutenticacionRepository;
import ar.edu.fi.unju.grupo04poo2020.service.UsuarioAutenticacionService;
import ar.edu.fi.unju.grupo04poo2020.util.MD5Util;

@Service
public class UsuarioAutenticacionServiceImp implements UsuarioAutenticacionService{
	private static final Charset UTF_8 = StandardCharsets.UTF_8;
	private ModelMapper mapper = new ModelMapper();
	@Autowired
	private UsuarioAutenticacionRepository usuarioRepo;
	@Autowired
	private EmpleadoRepository empleadoRepo;
	@Autowired
	private RolRepository rolRepository;

	@Override
	public UsuarioAutenticacionDto generarClave(UsuarioAutenticacionDto usuarioDto) {
		//if(usuarioRepo.findByEmpleado(usuarioDto.getEmpleado())!=null) {
			byte[] bytes = MD5Util.digest(usuarioDto.getPassword().getBytes(UTF_8));
			String pass = MD5Util.bytesToHex(bytes);
			Rol rol = rolRepository.findByRolId(usuarioDto.getRol());
			Empleado empleado= empleadoRepo.findByEmpleadoId(usuarioDto.getEmpleado());
			UsuarioAutenticacion usuario = new UsuarioAutenticacion(usuarioDto.getUsuario(), pass, usuarioDto.getActivo(), empleado, rol);
			//mapper.map(usuarioDto, usuario);
			usuarioRepo.save(usuario);
			return usuarioDto;
		/*}else {
			return null;
		}*/
	}

	@Override
	public EmpleadoDto login(String password) {
		byte[] bytes = MD5Util.digest(password.getBytes(UTF_8));
		String pass = MD5Util.bytesToHex(bytes);
		UsuarioAutenticacion usuarioLogin = usuarioRepo.findByPassword(pass);
		Empleado empleado = empleadoRepo.findByEmpleadoId(usuarioLogin.getEmpleado().getEmpleadoId());
		EmpleadoDto empleadoDto = new EmpleadoDto(empleado.getNombre(), empleado.getApellido(), empleado.getEmail(), empleado.getTelefono(), empleado.getFechaAlquiler(), empleado.getPuestoLaboral().getId(), empleado.getSalario(), empleado.getComision(), empleado.getManager().getEmpleadoId(), empleado.getDepartamento().getId());
		return empleadoDto;
	}

	@Override
	public UsuarioAutenticacionDto get(String usuario) {
		Optional<UsuarioAutenticacion> usuarioAut = usuarioRepo.findByUsuario(usuario);
		UsuarioAutenticacionDto usuarioDto = new UsuarioAutenticacionDto();
		mapper.map(usuarioAut,usuarioDto);
		return usuarioDto;
	}

}
