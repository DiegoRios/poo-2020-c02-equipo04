package ar.edu.fi.unju.grupo04poo2020.dto;


public class RegionDto {
	private Integer id;
	private String nombre;
	
	public RegionDto() {
		super();
	}
	public RegionDto(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
