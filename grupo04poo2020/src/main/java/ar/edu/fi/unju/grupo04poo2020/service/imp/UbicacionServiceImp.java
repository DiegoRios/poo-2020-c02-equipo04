package ar.edu.fi.unju.grupo04poo2020.service.imp;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import ar.edu.fi.unju.grupo04poo2020.dto.UbicacionDto;
import ar.edu.fi.unju.grupo04poo2020.entity.Ubicacion;
import ar.edu.fi.unju.grupo04poo2020.repository.UbicacionRepository;
import ar.edu.fi.unju.grupo04poo2020.service.UbicacionService;

public class UbicacionServiceImp implements UbicacionService{
	private ModelMapper mapper = new ModelMapper();
	@Autowired 
	private UbicacionRepository ubicacionRepo;

	@Override
	public UbicacionDto get(Integer id) {
		Optional<Ubicacion> ubicacion = ubicacionRepo.findById(id);
		UbicacionDto ubicacionDto = new UbicacionDto();
		mapper.map(ubicacion,ubicacionDto);
		return ubicacionDto;
	}

}
