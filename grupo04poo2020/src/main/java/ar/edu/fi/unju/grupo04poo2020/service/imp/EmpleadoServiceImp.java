package ar.edu.fi.unju.grupo04poo2020.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.entity.Departamento;
import ar.edu.fi.unju.grupo04poo2020.entity.Empleado;
import ar.edu.fi.unju.grupo04poo2020.entity.PuestoLaboral;
import ar.edu.fi.unju.grupo04poo2020.repository.DepartamentoRepository;
import ar.edu.fi.unju.grupo04poo2020.repository.EmpleadoRepository;
import ar.edu.fi.unju.grupo04poo2020.repository.PuestoLaboralRepository;
import ar.edu.fi.unju.grupo04poo2020.service.EmpleadoService;

@Service
public class EmpleadoServiceImp implements EmpleadoService{
	private ModelMapper mapper = new ModelMapper();
	@Autowired
	private EmpleadoRepository empleadoRepository;
	@Autowired 
	private PuestoLaboralRepository puestoRepo;
	@Autowired
	private HistoriaLaboralServiceImp historiaService;
	@Autowired
	private DepartamentoRepository departamentoRepo;
	@Override
	public EmpleadoDto get(Integer id) {
		Optional<Empleado> empleado = empleadoRepository.findById(id);
		EmpleadoDto empleadoDto = new EmpleadoDto();
		mapper.map(empleado,empleadoDto);
		return empleadoDto;
	}

	@Override
	public EmpleadoDto insert(EmpleadoDto empleadoDto) {
		//HistoriaLaboralDto historiaDto= new HistoriaLaboralDto(empleadoDto, new Date(), null, empleadoDto.getPuestoLaboral(), empleadoDto.getDepartamento());
		PuestoLaboral puesto = puestoRepo.findByPuestoLaboralId(empleadoDto.getPuestoLaboral());
		Departamento departamento = departamentoRepo.findByDepartamentoId(empleadoDto.getDepartamento());
		Empleado manager = empleadoRepository.findByEmpleadoId(empleadoDto.getManager());
 		Empleado empleado = new Empleado(empleadoDto.getNombre(), empleadoDto.getApellido(), empleadoDto.getEmail(), empleadoDto.getTelefono(), empleadoDto.getFechaAlquiler(), puesto, empleadoDto.getSalario(), empleadoDto.getComision(), manager,departamento);
		//mapper.map(empleadoDto, empleado);
		empleadoRepository.save(empleado);
		//historiaService.insert(historiaDto);
		return empleadoDto;
	}

	@Override
	public void update(EmpleadoDto empleadoDto) {
		//HistoriaLaboralDto historiaDto= new HistoriaLaboralDto(empleadoDto, new Date(), null, empleadoDto.getPuestoLaboral(), empleadoDto.getDepartamento());
		Empleado empleado = new Empleado();
		mapper.map(empleadoDto, empleado);
		empleadoRepository.save(empleado);
		//historiaService.insert(historiaDto);
	}

	public HistoriaLaboralServiceImp getHistoriaService() {
		return historiaService;
	}

	public void setHistoriaService(HistoriaLaboralServiceImp historiaService) {
		this.historiaService = historiaService;
	}

	@Override
	public List<EmpleadoDto> getSalaryByDepartment(Integer id) {
		Double suma = null;
		List<Empleado> empleados = empleadoRepository.findByDepartamento(id);
		for(Empleado e: empleados) {
			suma = suma + e.getSalario();
		}
		Double promedioSalario = suma/empleados.size();
		List<EmpleadoDto> empleadosSalario = new ArrayList<EmpleadoDto>();
		for(Empleado empleado: empleados) {
			if(empleado.getSalario() > promedioSalario) {
				EmpleadoDto empleadoDto = new EmpleadoDto();
				mapper.map(empleado, empleadoDto);
				empleadosSalario.add(empleadoDto);
			}
		}
		return empleadosSalario;
	}


}
