package ar.edu.fi.unju.grupo04poo2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Grupo04poo2020Application {

	public static void main(String[] args) {
		SpringApplication.run(Grupo04poo2020Application.class, args);
	}

}
