package ar.edu.fi.unju.grupo04poo2020.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "countries")
public class Pais {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="country_id")
	private String id;
	@Column(name="country_name")
	private String nombre;
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="region_id")
	private Region region;
	
	
	
	public Pais() {
		super();
	}



	public Pais(String id, String nombre, Region region) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.region = region;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public Region getRegion() {
		return region;
	}



	public void setRegion(Region region) {
		this.region = region;
	}
	
	
}
