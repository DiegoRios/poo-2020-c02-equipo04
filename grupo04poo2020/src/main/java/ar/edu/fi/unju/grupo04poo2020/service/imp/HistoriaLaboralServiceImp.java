package ar.edu.fi.unju.grupo04poo2020.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.dto.HistoriaLaboralDto;
import ar.edu.fi.unju.grupo04poo2020.entity.Empleado;
import ar.edu.fi.unju.grupo04poo2020.entity.HistoriaLaboral;
import ar.edu.fi.unju.grupo04poo2020.repository.EmpleadoRepository;
import ar.edu.fi.unju.grupo04poo2020.repository.HistoriaLaboralRepository;
import ar.edu.fi.unju.grupo04poo2020.service.HistoriaLaboralService;

@Service
public class HistoriaLaboralServiceImp implements HistoriaLaboralService{
	private ModelMapper mapper = new ModelMapper();
	@Autowired
	private HistoriaLaboralRepository historiaRepo;
	@Autowired
	private EmpleadoRepository empleadoRepo;

	@Override
	public List<EmpleadoDto> getEmpleados(Integer id) {
		List<Empleado> empleados =  historiaRepo.findByEmpleado(empleadoRepo.getOne(id));
		List<EmpleadoDto> empleadosDto = new ArrayList<EmpleadoDto>();
		for(Empleado e: empleados) {
			EmpleadoDto empleadoDto = new EmpleadoDto();
			mapper.map(e,empleadoDto);
			empleadosDto.add(empleadoDto);
		}
		return empleadosDto;
	}

	@Override
	public HistoriaLaboralDto insert(HistoriaLaboralDto historiaDto) {
		HistoriaLaboral historia = new HistoriaLaboral();
		mapper.map(historiaDto, historia);
		historiaRepo.save(historia);
		return historiaDto;
	}

}
