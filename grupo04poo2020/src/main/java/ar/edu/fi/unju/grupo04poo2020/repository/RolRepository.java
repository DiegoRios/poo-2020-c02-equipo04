package ar.edu.fi.unju.grupo04poo2020.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.fi.unju.grupo04poo2020.entity.Rol;

public interface RolRepository extends JpaRepository<Rol, Integer>{
	Optional<Rol> findById(Integer id);
	Rol findByRolId(Integer id);

}
