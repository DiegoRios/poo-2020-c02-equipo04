package ar.edu.fi.unju.grupo04poo2020.dto;

public class DepartamentoDto {
	private Integer id;
	private String nombre;
	private UbicacionDto ubicacion;
	private Integer manager;
	
	public DepartamentoDto() {
		super();
	}

	public DepartamentoDto(Integer id, String nombre, UbicacionDto ubicacion, Integer manager) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		this.manager = manager;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public UbicacionDto getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(UbicacionDto ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Integer getManager() {
		return manager;
	}

	public void setManager(Integer manager) {
		this.manager = manager;
	}
	
	

}
