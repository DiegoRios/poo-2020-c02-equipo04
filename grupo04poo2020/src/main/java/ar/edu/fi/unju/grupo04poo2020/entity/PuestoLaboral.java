package ar.edu.fi.unju.grupo04poo2020.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jobs")
public class PuestoLaboral {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="job_id")
	private String puestoLaboralId;
	@Column(name = "job_title")
	private String titulo;
	@Column(name = "min_salary")
	private Double salarioMinimo;
	@Column(name = "max_salary")
	private Double salarioMaximo;
	
	public PuestoLaboral() {
		super();
	}

	public PuestoLaboral(String id, String titulo, Double salarioMinimo, Double salarioMaximo) {
		super();
		this.puestoLaboralId = id;
		this.titulo = titulo;
		this.salarioMinimo = salarioMinimo;
		this.salarioMaximo = salarioMaximo;
	}

	public String getId() {
		return puestoLaboralId;
	}

	public void setId(String id) {
		this.puestoLaboralId = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Double getSalarioMinimo() {
		return salarioMinimo;
	}

	public void setSalarioMinimo(Double salarioMinimo) {
		this.salarioMinimo = salarioMinimo;
	}

	public Double getSalarioMaximo() {
		return salarioMaximo;
	}

	public void setSalarioMaximo(Double salarioMaximo) {
		this.salarioMaximo = salarioMaximo;
	}
	
	

}
