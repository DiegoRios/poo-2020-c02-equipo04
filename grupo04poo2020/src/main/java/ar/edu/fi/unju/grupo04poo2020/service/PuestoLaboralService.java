package ar.edu.fi.unju.grupo04poo2020.service;

import ar.edu.fi.unju.grupo04poo2020.dto.PuestoLaboralDto;

public interface PuestoLaboralService {
	PuestoLaboralDto get(String id);

}
