package ar.edu.fi.unju.grupo04poo2020.dto;

import java.io.Serializable;

public class PuestoLaboralDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;
	private String titulo;
	private Double salarioMinimo;
	private Double salarioMaximo;
	
	
	
	public PuestoLaboralDto() {
		super();
	}



	public PuestoLaboralDto(String id, String titulo, Double salarioMinimo, Double salarioMaximo) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.salarioMinimo = salarioMinimo;
		this.salarioMaximo = salarioMaximo;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getTitulo() {
		return titulo;
	}



	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}



	public Double getSalarioMinimo() {
		return salarioMinimo;
	}



	public void setSalarioMinimo(Double salarioMinimo) {
		this.salarioMinimo = salarioMinimo;
	}



	public Double getSalarioMaximo() {
		return salarioMaximo;
	}



	public void setSalarioMaximo(Double salarioMaximo) {
		this.salarioMaximo = salarioMaximo;
	}
	
	
	
	

}
