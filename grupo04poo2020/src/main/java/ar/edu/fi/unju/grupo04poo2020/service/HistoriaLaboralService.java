package ar.edu.fi.unju.grupo04poo2020.service;

import java.util.List;


import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.dto.HistoriaLaboralDto;
import ar.edu.fi.unju.grupo04poo2020.entity.Empleado;

public interface HistoriaLaboralService {
	List<EmpleadoDto> getEmpleados(Integer id);
	HistoriaLaboralDto insert(HistoriaLaboralDto historiaDto);

}
