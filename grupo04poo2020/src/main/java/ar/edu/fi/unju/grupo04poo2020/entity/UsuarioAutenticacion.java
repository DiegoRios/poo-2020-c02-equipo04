package ar.edu.fi.unju.grupo04poo2020.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "users_autentication")
public class UsuarioAutenticacion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="user_id")
	private Integer id;
	@Column(name = "username")
	private String usuario;
	@Column(name = "password")
	private String password;
	@Column(name = "active")
	private boolean activo;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employee_id")
	private Empleado empleado;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rol_id")
	private Rol rol;
	
	public UsuarioAutenticacion() {
		super();
	}

	
	
	public UsuarioAutenticacion(String usuario, String password, boolean activo, Empleado empleado, Rol rol) {
		super();
		this.usuario = usuario;
		this.password = password;
		this.activo = activo;
		this.empleado = empleado;
		this.rol = rol;
	}



	public UsuarioAutenticacion(Integer id, String usuario, String password, boolean activo, Empleado empleado,
			Rol rol) {
		super();
		this.id = id;
		this.usuario = usuario;
		this.password = password;
		this.activo = activo;
		this.empleado = empleado;
		this.rol = rol;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}
	
	

}
