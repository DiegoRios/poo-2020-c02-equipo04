package ar.edu.fi.unju.grupo04poo2020.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Rol {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="rol_id")
	private Integer rolId;
	@Column(name = "rol_name")
	private String nombre;
	
	public Rol() {
		super();
	}
	
	public Rol(Integer id, String nombre) {
		super();
		this.rolId = id;
		this.nombre = nombre;
	}

	public Integer getId() {
		return rolId;
	}

	public void setId(Integer id) {
		this.rolId = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
