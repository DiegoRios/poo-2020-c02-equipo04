package ar.edu.fi.unju.grupo04poo2020.service;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.dto.UsuarioAutenticacionDto;

public interface UsuarioAutenticacionService {
	UsuarioAutenticacionDto generarClave(UsuarioAutenticacionDto usuarioDto);
	EmpleadoDto login(String password); 
	UsuarioAutenticacionDto get(String usuario);
}
