package ar.edu.fi.unju.grupo04poo2020.view.util;

import java.util.List;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;

public abstract class Informe {
	public static final String PATH_INFORME = "tmp/"; 
	public abstract Boolean generarInforme(EmpleadoDto empleadoDto);
	public abstract Boolean generarInformeExcel(List<EmpleadoDto> empleados);
}
