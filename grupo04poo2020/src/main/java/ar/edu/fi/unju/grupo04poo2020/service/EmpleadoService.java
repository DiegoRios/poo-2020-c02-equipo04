package ar.edu.fi.unju.grupo04poo2020.service;


import java.util.List;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;

public interface EmpleadoService {
	EmpleadoDto get(Integer id); 

	EmpleadoDto insert(EmpleadoDto empleadoDto);
	
	void update(EmpleadoDto empleadoDto);
	
	List<EmpleadoDto> getSalaryByDepartment(Integer id);

}
