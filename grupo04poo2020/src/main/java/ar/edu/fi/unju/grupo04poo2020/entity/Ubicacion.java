package ar.edu.fi.unju.grupo04poo2020.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "locations")
public class Ubicacion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="location_id")
	private Integer id;
	@Column(name="street_address")
	private String direccion;
	@Column(name="postal_code")
	private String codigoPostal;
	@Column(name="city")
	private String cuidad;
	@Column(name = "state_province")
	private String provinciaEstado;
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "country_id")
	private Pais pais;
	
	public Ubicacion() {
		super();
	}

	public Ubicacion(Integer id, String direccion, String codigoPostal, String cuidad, String provinciaEstado, Pais pais) {
		super();
		this.id = id;
		this.direccion = direccion;
		this.codigoPostal = codigoPostal;
		this.cuidad = cuidad;
		this.provinciaEstado = provinciaEstado;
		this.pais = pais;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getCuidad() {
		return cuidad;
	}

	public void setCuidad(String cuidad) {
		this.cuidad = cuidad;
	}

	public String getProvinciaEstado() {
		return provinciaEstado;
	}

	public void setProvinciaEstado(String provinciaEstado) {
		this.provinciaEstado = provinciaEstado;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	
	
}
