package ar.edu.fi.unju.grupo04poo2020.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "departments")
public class Departamento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="department_id")
	private Integer departamentoId;
	@Column(name = "department_name")
	private String nombre;
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id")
	private Ubicacion ubicacion;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "manager_id")
	private Empleado manager;
	
	public Departamento() {
		// TODO Auto-generated constructor stub
	}

	public Departamento(Integer id, String nombre, Ubicacion ubicacion,
			Empleado manager) {
		super();
		this.departamentoId = id;
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		this.manager = manager;
	}

	public Integer getId() {
		return departamentoId;
	}

	public void setId(Integer id) {
		this.departamentoId = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Ubicacion getUbicacion() {
		
		return ubicacion;
	}

	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Empleado getManager() {
		return manager;
	}

	public void setManager(Empleado manager) {
		this.manager = manager;
	}
	
	
	
}
