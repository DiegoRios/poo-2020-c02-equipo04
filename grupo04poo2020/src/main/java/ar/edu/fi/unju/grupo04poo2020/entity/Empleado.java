package ar.edu.fi.unju.grupo04poo2020.entity;

import java.util.Date;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class Empleado {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="employee_id")
	private Integer empleadoId;
	@Column(name = "first_name")
	private String nombre;
	@Column(name = "last_name")
	private String apellido;
	@Column(name = "email")
	private String email;
	@Column(name = "phone_number")
	private String telefono;
	@Column(name = "hire_date")
	private Date fechaAlquiler;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_id")
	private PuestoLaboral puestoLaboral;
	@Column(name = "salary")
	private Double salario;
	@Column(name = "commission_pct")
	private Double comision;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "manager_id")
	private Empleado manager;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_id")
	private Departamento departamento;
	
	public Empleado() {
		// TODO Auto-generated constructor stub
	}

	
	public Empleado(String nombre, String apellido, String email, String telefono, Date fechaAlquiler,
			PuestoLaboral puestoLaboral, Double salario, Double comision, Empleado manager, Departamento departamento) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.fechaAlquiler = fechaAlquiler;
		this.puestoLaboral = puestoLaboral;
		this.salario = salario;
		this.comision = comision;
		this.manager = manager;
		this.departamento = departamento;
	}
	
	


	/*public Empleado(String nombre, String apellido, String email, String telefono, Date fechaAlquiler,
			Optional<PuestoLaboral> puesto, Double salario, Double comision, Optional<Empleado> manager2, Optional<Departamento> departamento2) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.fechaAlquiler = fechaAlquiler;
		this.puestoLaboral = puesto;
		this.salario = salario;
		this.comision = comision;
		this.manager = manager2;
		this.departamento = departamento2;
	}*/


	public Integer getEmpleadoId() {
		return empleadoId;
	}

	public void setId(Integer empleadoId) {
		this.empleadoId = empleadoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getFechaAlquiler() {
		return fechaAlquiler;
	}

	public void setFechaAlquiler(Date fechaAlquiler) {
		this.fechaAlquiler = fechaAlquiler;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public Double getComision() {
		return comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}


	public PuestoLaboral getPuestoLaboral() {
		return puestoLaboral;
	}


	public void setPuestoLaboral(PuestoLaboral puestoLaboral) {
		this.puestoLaboral = puestoLaboral;
	}


	public Empleado getManager() {
		return manager;
	}


	public void setManager(Empleado manager) {
		this.manager = manager;
	}


	public Departamento getDepartamento() {
		return departamento;
	}

    public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
		
    }
    
	/*public Optional<PuestoLaboral> getPuestoLaboral() {
		return puestoLaboral;
	}

	public void setPuestoLaboral(Optional<PuestoLaboral> puestoLaboral) {
		this.puestoLaboral = puestoLaboral;
	}

	public Optional<Departamento> getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Optional<Departamento> departamento) {
		this.departamento = departamento;
	}

	public Optional<Empleado> getManager() {
		return manager;
	}

	public void setManager(Empleado manager) {
		this.manager = manager;
	}*/
}
