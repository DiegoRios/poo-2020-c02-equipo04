package ar.edu.fi.unju.grupo04poo2020.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.fi.unju.grupo04poo2020.dto.DepartamentoDto;
import ar.edu.fi.unju.grupo04poo2020.entity.Departamento;
import ar.edu.fi.unju.grupo04poo2020.repository.DepartamentoRepository;
import ar.edu.fi.unju.grupo04poo2020.service.DepartamentoService;

@Service
public class DepartamentoServiceImp implements DepartamentoService{
	private ModelMapper mapper = new ModelMapper();
	@Autowired
	private DepartamentoRepository departamentoRepo;

	@Override
	public DepartamentoDto get(Integer id) {
		Optional<Departamento> departamento= departamentoRepo.findById(id);
		DepartamentoDto departamentoDto = new DepartamentoDto();
		mapper.map(departamento,departamentoDto);
		return departamentoDto;
	}

	@Override
	public List<DepartamentoDto> findByManager(Integer id) {
		List<Departamento> departamentos = departamentoRepo.findByManager(id);
		List<DepartamentoDto> departamentosDto= new ArrayList<DepartamentoDto>();
		for(Departamento dpto: departamentos) {
			DepartamentoDto departamentoDto =new DepartamentoDto();
			mapper.map(dpto,departamentoDto);
			departamentosDto.add(departamentoDto);
		}
		return departamentosDto;
	}

}
