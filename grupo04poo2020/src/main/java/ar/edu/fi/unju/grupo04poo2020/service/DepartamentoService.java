package ar.edu.fi.unju.grupo04poo2020.service;

import java.util.List;

import ar.edu.fi.unju.grupo04poo2020.dto.DepartamentoDto;

public interface DepartamentoService {
	DepartamentoDto get(Integer id);
	List<DepartamentoDto> findByManager(Integer id);

}
