package ar.edu.fi.unju.grupo04poo2020.view.util;

import java.io.FileOutputStream;
import java.util.List;

import javax.inject.Named;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;

@Named
public class InformePdf extends Informe {
	private static String FILE = PATH_INFORME + "InformeEmpleados.pdf";
	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
	private static Font headerTableFont = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

	@Override
	public Boolean generarInforme(EmpleadoDto empleadoDto) {
		try {
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(FILE));
			document.open();
			addMetaData(document);
			addCabecera(empleadoDto, document);
			document.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static void addMetaData(Document document) {
		document.addTitle("Listado de Empleados");
		document.addSubject("Empleados");
		document.addKeywords("Java, PDF, iText");
		document.addAuthor("Agustina Mamani");
		document.addCreator("Agustina Mamani");
	}

	private static void addCabecera(EmpleadoDto empleadoDto, Document document) throws DocumentException{
		Paragraph preface = new Paragraph();
		addEmptyLine(preface, 1);
		preface.add(new Paragraph("Informe de Empleado", catFont));
		addEmptyLine(preface, 1);
		preface.add(new Paragraph("Nombre: " + empleadoDto.getNombre() + " - Apellido: " + empleadoDto.getApellido(),smallBold));
		preface.add(new Paragraph("Email: " + empleadoDto.getEmail() + " - Telofono: "+ empleadoDto.getTelefono(), smallBold));
		preface.add(new Paragraph("Salario: " + empleadoDto.getTelefono() + " - Comision: " + empleadoDto.getComision(),smallBold));
		document.add(preface);
	}

	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	@Override
	public Boolean generarInformeExcel(List<EmpleadoDto> empleados) {
		// TODO Auto-generated method stub
		return null;
	}

}
