package ar.edu.fi.unju.grupo04poo2020;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.service.InformeService;
import ar.edu.fi.unju.grupo04poo2020.service.imp.EmpleadoServiceImp;
import ar.edu.fi.unju.grupo04poo2020.view.util.InformeExcel;
import ar.edu.fi.unju.grupo04poo2020.view.util.InformePdf;

@SpringBootTest
@RunWith(JUnitPlatform.class)
class InformeTest {
	
	@Autowired
	private InformeService informeService;	
	@Autowired
	private EmpleadoServiceImp empleadoService;
	private EmpleadoDto empleadoDto;

	@BeforeEach
	void setUp() throws Exception {
		empleadoDto = new EmpleadoDto();
	}

	@AfterEach
	void tearDown() throws Exception {
		empleadoDto = null;
	}
	

	void testInformePdf() {
		empleadoDto	= empleadoService.get(100);
		informeService.setInforme(new InformePdf());
		assertNotNull(informeService.generarInforme(empleadoDto));
	}

	@Test
	void testInformeExcel() {
		List<EmpleadoDto> empleados = empleadoService.getSalaryByDepartment(110);
		informeService.setInforme(new InformeExcel());
		assertNotNull(informeService.generarInformeExcel(empleados));
	}

}
