package ar.edu.fi.unju.grupo04poo2020;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.fi.unju.grupo04poo2020.dto.PuestoLaboralDto;
import ar.edu.fi.unju.grupo04poo2020.dto.RolDto;
import ar.edu.fi.unju.grupo04poo2020.service.imp.PuestoLaboralServiceImp;
import ar.edu.fi.unju.grupo04poo2020.service.imp.RolServiceImp;



@SpringBootTest
@RunWith(JUnitPlatform.class)
class RolRepositoryTest {
	
	@Autowired
	private RolServiceImp  rolService;
	private RolDto rolDto;
	@Autowired
	private PuestoLaboralServiceImp puestoService;		
	private PuestoLaboralDto puesto;

	@BeforeEach
	void setUp() throws Exception {
		rolDto = new RolDto();
	}

	@AfterEach
	void tearDown() throws Exception {
		rolDto = null;
	}

	@Test
	void testInsert() {
		/*rolDto = new RolDto(1, "Administrador");
		rolService.insert(rolDto);
		
		assertNotNull(rolService.get(1));*/
		puesto = puestoService.get("AC_MGR");
		assertNotNull(puesto);
	}



}
