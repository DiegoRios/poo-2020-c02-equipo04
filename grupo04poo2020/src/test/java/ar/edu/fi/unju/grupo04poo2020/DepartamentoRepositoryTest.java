package ar.edu.fi.unju.grupo04poo2020;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.fi.unju.grupo04poo2020.dto.DepartamentoDto;
import ar.edu.fi.unju.grupo04poo2020.entity.Departamento;
import ar.edu.fi.unju.grupo04poo2020.repository.DepartamentoRepository;
import ar.edu.fi.unju.grupo04poo2020.service.imp.DepartamentoServiceImp;


@SpringBootTest
@RunWith(JUnitPlatform.class)
class DepartamentoRepositoryTest {
	@Autowired
	private DepartamentoRepository departamentoRepository;
	@Autowired
	private DepartamentoServiceImp departamentoService;
	private DepartamentoDto departamentoDto;

	@BeforeEach
	void setUp() throws Exception {
		departamentoDto = new DepartamentoDto();
	}

	@AfterEach
	void tearDown() throws Exception {
		departamentoDto = null;
	}

	@Test
	void test() {
		departamentoDto = departamentoService.get(10);
		assertNotNull(departamentoDto);
	}
	
	
	void buscarDepartamentosPorManangerTest() {
		List<Departamento> departamentos = departamentoRepository.findByManager(100);
		assertEquals(1, departamentos.size());
	}


}
