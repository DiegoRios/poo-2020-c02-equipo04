package ar.edu.fi.unju.grupo04poo2020;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.dto.HistoriaLaboralDto;
import ar.edu.fi.unju.grupo04poo2020.dto.PuestoLaboralDto;
import ar.edu.fi.unju.grupo04poo2020.dto.RolDto;
import ar.edu.fi.unju.grupo04poo2020.service.imp.HistoriaLaboralServiceImp;
import ar.edu.fi.unju.grupo04poo2020.service.imp.PuestoLaboralServiceImp;
import ar.edu.fi.unju.grupo04poo2020.service.imp.RolServiceImp;



@SpringBootTest
@RunWith(JUnitPlatform.class)
class HistoricoRepositoryTest {
	
	@Autowired
	private HistoriaLaboralServiceImp  historicoService;
	private HistoriaLaboralDto historicoDto;

	@BeforeEach
	void setUp() throws Exception {
		historicoDto = new HistoriaLaboralDto();
	}

	@AfterEach
	void tearDown() throws Exception {
		historicoDto = null;
	}
	
    @Test
	void testGetHistoricos() {
		List<EmpleadoDto> empleadosHistoricos = historicoService.getEmpleados(101);
		
		assertEquals(2, empleadosHistoricos.size());
		
		
	}



}
