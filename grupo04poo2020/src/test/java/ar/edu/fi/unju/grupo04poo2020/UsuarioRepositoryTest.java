package ar.edu.fi.unju.grupo04poo2020;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.dto.UsuarioAutenticacionDto;
import ar.edu.fi.unju.grupo04poo2020.service.imp.UsuarioAutenticacionServiceImp;



@RunWith(JUnitPlatform.class)
@SpringBootTest
class UsuarioRepositoryTest {
	
	@Autowired
	private UsuarioAutenticacionServiceImp  usuarioService;
	private UsuarioAutenticacionDto usuarioDto;

	
	@BeforeEach
	void setUp() throws Exception {
		usuarioDto = new  UsuarioAutenticacionDto();
	}

	@AfterEach
	void tearDown() throws Exception {
		usuarioDto = null;
	}

	@Test
	void testGenerarClave() {
		usuarioDto = new UsuarioAutenticacionDto("usuario45", "4321", true, 195, 1);
	    usuarioService.generarClave(usuarioDto);
	    assertNotNull(usuarioService.get("usuario12"));			
	}
	
	@Test
	void loginUsuario() {
		EmpleadoDto empleado = usuarioService.login("4321");
		assertNotNull(empleado);
	}


}
