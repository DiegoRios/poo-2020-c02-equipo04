package ar.edu.fi.unju.grupo04poo2020;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.fi.unju.grupo04poo2020.dto.DepartamentoDto;
import ar.edu.fi.unju.grupo04poo2020.dto.EmpleadoDto;
import ar.edu.fi.unju.grupo04poo2020.dto.PuestoLaboralDto;
import ar.edu.fi.unju.grupo04poo2020.service.imp.DepartamentoServiceImp;
import ar.edu.fi.unju.grupo04poo2020.service.imp.EmpleadoServiceImp;
import ar.edu.fi.unju.grupo04poo2020.service.imp.PuestoLaboralServiceImp;

@RunWith(JUnitPlatform.class)
@SpringBootTest
class EmpleadoRepositoryTest {
	
	@Autowired
	private EmpleadoServiceImp empleadoService;
	private EmpleadoDto empleadoDto;
	@Autowired
	private PuestoLaboralServiceImp puestoService;
	@Autowired
	private DepartamentoServiceImp departamentoService;
	@BeforeEach
	void setUp() throws Exception {
		empleadoDto = new EmpleadoDto();	
	}

	@AfterEach
	void tearDown() throws Exception {
		empleadoDto = null;
	}


	void testEmpleado() {
		empleadoDto = empleadoService.get(100);
		assertNotNull(empleadoDto);
	}
	
	@Test
	void testInsertEmpleado() {
		/*PuestoLaboralDto puesto = puestoService.get("AC_MGR");
		DepartamentoDto departamento = departamentoService.get(10);
		EmpleadoDto manager = empleadoService.get(147);*/
		
		empleadoDto = new EmpleadoDto("nombre1", "apellido1", "email45", "4512", new Date(), "PR_REP", 4544.0, null, 160, 250);
		//empleadoDto = new EmpleadoDto("nombre", "apelldio", "email", "45123", new Date(), "AC_MGR", 4500.4, null, 100, 10);
		assertNotNull(empleadoService.insert(empleadoDto));
	
	}

	@Test
	void testEmpleadoPorDepartamentos() {
		
	}
	void buscarEmpleadosPorManangerTest() {
		//List<Empleado> empleados=empleadoRepository.findByManager((long) 120);
		//assertTrue(empleados.size()>0);
	}

}
